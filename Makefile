SHELL := /bin/bash
PROJECT := $(shell basename $$(pwd))
TAGS := -tags "test mocks"

build:
	CGO_ENABLED=0 go build -a

test:
	go test -v $$(glide novendor) $(TAGS)

install-deps:
	clear
	glide i

golint:
	golint=$(shell which golint)
	[ ! -z $${golint} ] || go get -v github.com/golang/lint/golint

lint: golint
	RESULT=""; \
	for i in $$(go list ./... | grep -v /vendor/); do \
		LINT="$$(golint $${i} | grep -v 'comment')"; \
		if [ ! -z "$${LINT}" ]; then \
			RESULT="$${RESULT}\n$${LINT}"; \
		fi \
	done; \
	echo -e "$${RESULT}"; \
	[ -z "$${RESULT}" ] || exit 1

fmt:
	RESULT="$$(gofmt -d -e $$(find . -type f -name '*.go' -not -path "./vendor/*"))" && \
	echo "$${RESULT}"; \
	[ -z "$${RESULT}" ] || exit 1

vet:
	go vet $(TAGS) $$(glide nv)

format-project:
	gofmt -w $$(find . -type f -name '*.go' -not -path "./vendor/*")

check-syntax: lint vet fmt

glide:
	[ $$(which glide) ] || curl https://glide.sh/get | sh

fswatch:
	[ $$(which fswatch) ] || go get -u -v github.com/codeskyblue/fswatch

circle:
	[ $$(which circleci) ] || curl -o /usr/local/bin/circleci https://circle-downloads.s3.amazonaws.com/releases/build_agent_wrapper/circleci && chmod +x /usr/local/bin/circleci

dev: glide fswatch
	fswatch

test-ci: circle
	circleci build

.PHONY: build test
