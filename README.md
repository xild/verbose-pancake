
```
           VERBOSE┊PANCAKE┊
┊┊┊┊╱▔▔▔▔▔▔╲┊┊┊┊
┊╱▔╱┈╭╮┈┈╭╮┈╲▔╲┊
╱╱▕┈┈┏━━━━┓┈┈▏╲╲
▏▏▕╲┈╰━━━━╯┈╱▏▕▕
▏▏┈╲╲▂▂▂▂▂▂╱╱┈▕▕
╲╲┈┈╲▂▂▂▂▂▂╱┈┈╱╱
```

## DESCRIPTION

This project just take a look at a data set [geoData.csv](./resource/geoData.csv)
and output the furthest and closest locations compared to a lat/lon provided in [main.go](main.go#L16). Also output two images [closestArea](./closest.png) and
[furthestArea](./furthest.png).


## PERFORMANCE
Every line is processed to avoid unnecessary memory allocation.

Using this [CSV](https://api.safecast.org/system/measurements.csv) with 9.8gb took around 4 minutes to complete
the task.


With the given [CSV](./resource/geoData.csv) took around 0,4ms to complete.

## HOW TO RUN

clone this project and type

 `$ make install-deps`

 `$ make`

 `$ ./verbose-pancake`


## HOW TO TEST

CI flow:
`make test-ci`

local: `make test`

## To dev
`make fswatch`
