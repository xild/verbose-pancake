package draw

import (
	"github.com/flopp/go-staticmaps"
	"github.com/golang/geo/s2"
	"github.com/fogleman/gg"
	"bitbucket.org/xild/verbose-pancake/model"
	"image/color"
	"errors"
)

var (
	EmptyLocationError = errors.New("Empty locations error. Don't need to generated a empty point map")
	NameNotFoundError  = errors.New("Name not found. Provide one name to generate a map ")
)

type (
	MapDraw interface {
		GenerateMap(locations []model.Location, name string)
	}

	MapDrawer struct {
		defaultLocation      s2.LatLng
		defaultLocationColor color.RGBA
		colorDefault         color.RGBA
		width                int
		height               int
	}
)

func Init(lon float64, lat float64, width int, height int) *MapDrawer {
	m := &MapDrawer{}
	m.defaultLocation = s2.LatLngFromDegrees(lat, lon)
	m.defaultLocationColor = color.RGBA{255, 153, 51, 0xff}
	m.colorDefault = color.RGBA{0xff, 0, 0, 0xff}
	m.width = width
	m.height = height
	return m
}

func (mD *MapDrawer) GenerateMap(locations []model.Location, name string) error {
	if len(locations) == 0 {
		return EmptyLocationError
	}

	if name == "" {
		return NameNotFoundError
	}

	ctx := sm.NewContext()
	ctx.SetSize(mD.width, mD.height)
	ctx.AddMarker(sm.NewMarker(mD.defaultLocation, mD.defaultLocationColor, 30.0))
	for _, f := range locations {
		ctx.AddMarker(sm.NewMarker(s2.LatLngFromDegrees(f.Latitude, f.Longitude), mD.colorDefault, 16.0))
	}

	img, err := ctx.Render()
	if err != nil {
		panic(err)
	}

	if err := gg.SavePNG(name, img); err != nil {
		panic(err)
	}

	return nil
}
