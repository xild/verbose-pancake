package draw

import (
	"image/color"
	"testing"

	"bitbucket.org/xild/verbose-pancake/model"
	"github.com/golang/geo/s2"
	"os"
)

const defaultLatitude float64 = 51.925146
const defaultLongitude float64 = 4.478617

var orange = color.RGBA{255, 153, 51, 0xff}
var red = color.RGBA{0xff, 0, 0, 0xff}

var locations = []model.Location{
	{ID: 382582, Latitude: 37.1768672, Longitude: -3.608897},
	{ID: 482365, Latitude: 52.36461880000000235, Longitude: 4.93169289999999982},
	{ID: 28403, Latitude: 51.9245615, Longitude: 4.492032399999999},
	{ID: 1098606, Latitude: 52.1542113, Longitude: 4.491864899999999},
	{ID: 24389, Latitude: 50.85132920000000212, Longitude: 5.70318029999999965},
	{ID: 19880, Latitude: 53.2315726000000069, Longitude: 6.52567179999999958},
	{ID: 436807, Latitude: 51.9224817, Longitude: 4.5140063},
	{ID: 420178, Latitude: 50.84924139999999682, Longitude: 5.6903737999999997},
	{ID: 22238, Latitude: 50.8444264, Longitude: 5.7156861},
	{ID: 505868, Latitude: 52.09791479999999808, Longitude: 5.11686619999999959},
	{ID: 381769, Latitude: 52.2934316, Longitude: 4.9934547},
	{ID: 419117, Latitude: 48.8653063, Longitude: 2.3794788},
	{ID: 23928, Latitude: 50.8651879, Longitude: 5.707368199999999},
	{ID: 1049729, Latitude: 52.37165040000000005, Longitude: 4.90306019999999965},
	{ID: 90872, Latitude: 52.06214569999999, Longitude: 4.235672099999999}}

func TestMapDrawer_GenerateMap(t *testing.T) {
	type expected struct {
		err error
	}
	type fields struct {
		defaultLocation      s2.LatLng
		defaultLocationColor color.RGBA
		colorDefault         color.RGBA
		width                int
		height               int
	}
	type args struct {
		locations []model.Location
		name      string
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		expected
	}{

		{
			"check file generation",
			fields{
				defaultLocation:      s2.LatLngFromDegrees(defaultLatitude, defaultLongitude),
				defaultLocationColor: orange,
				colorDefault:         red,
				width:                1000,
				height:               1000,

			},
			args{
				locations,
				"test.png",
			},
			expected{nil},
		},
		{
			"check file generation with EmptyLocationError",
			fields{
				defaultLocation:      s2.LatLngFromDegrees(defaultLatitude, defaultLongitude),
				defaultLocationColor: orange,
				colorDefault:         red,
				width:                1000,
				height:               1000,

			},
			args{
				nil,
				"test.png",
			},
			expected{EmptyLocationError},
		},

		{
			"check file generation with NameNotFoundError",
			fields{
				defaultLocation:      s2.LatLngFromDegrees(defaultLatitude, defaultLongitude),
				defaultLocationColor: orange,
				colorDefault:         red,
				width:                1000,
				height:               1000,

			},
			args{
				locations,
				"",
			},
			expected{NameNotFoundError},
		},

	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mD := &MapDrawer{
				defaultLocation:      tt.fields.defaultLocation,
				defaultLocationColor: tt.fields.defaultLocationColor,
				colorDefault:         tt.fields.colorDefault,
				width:                tt.fields.width,
				height:               tt.fields.height,
			}
			err := mD.GenerateMap(tt.args.locations, tt.args.name)

			if err != tt.expected.err {
				t.Fatalf("Expected %v but %v", tt.expected.err, err)
			}

			if err != nil {
				return
			}

			if _, err := os.Stat(tt.args.name); os.IsNotExist(err) {
				t.Fatal("Expected a image creation file instead not found")
			} else {
				//remove file :)
				var err = os.Remove(tt.args.name)
				if err != nil {
					return
				}
			}

		})
	}
}
