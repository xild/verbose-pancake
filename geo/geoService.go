package geo

import (
	"math"
	"fmt"
)

const (
	earthRadius    float64 = 6378100
	latitudeError          = "Latitude should be between -90 and 90 input=%f"
	longitudeError         = "Longitude should be between -90 and 90 input=%f"
)

//Calculate the distance between 2 coordinate.
//return result in meter or an error
func Distance(lat1, lon1, lat2, lon2 float64) (float64, error) {
	var la1, lo1, la2, lo2 float64

	if err := validateLatitude(lat1, lat2); err != nil {
		return 0, err
	}

	if err := validateLongitude(lon1, lon2); err != nil {
		return 0, err
	}
	
	la1 = toRadians(lat1)
	lo1 = toRadians(lon1)
	la2 = toRadians(lat2)
	lo2 = toRadians(lon2)

	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)

	return 2 * earthRadius * math.Asin(math.Sqrt(h)), nil
}

func toRadians(l float64) float64 {
	return l * math.Pi / 180
}

func hsin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}

//Valid latitude must be a number between -90 and 90
func validateLatitude(latitude ...float64) error {
	for _, l := range latitude {
		if l < -90 || l > 90 {
			return fmt.Errorf(latitudeError, l)
		}
	}
	return nil
}

//Valid longitude must be a number between -180 and 180.
func validateLongitude(longitude ...float64) error {
	for _, l := range longitude {
		if l < -180 || l > 180 {
			return fmt.Errorf(longitudeError, l)
		}
	}
	return nil
}
