package geo

import (
	"testing"
)

const officeLatitude float64 = 51.925146
const officeLongitude float64 = 4.478617

//The distance between amsterdam and rotterdam
func TestDistance_should_be_57km(t *testing.T) {
	var distancesTable = []struct {
		latitude  float64
		longitude float64
		distance  float64
	}{
		{52.36461880000000235, 4.93169289999999982, 57889.95008817888},
		{52.1542113, 4.491864899999999, 25515.415069981194},
	}

	for _, d := range distancesTable {
		if meters, err := Distance(d.latitude, d.longitude, officeLatitude, officeLongitude); err != nil {
			t.Error(err)
		} else {
			if meters != d.distance {
				t.Errorf("Invalid expected distance. Expected %=v Output=%v", d.distance, meters)
			}
		}
	}

}

//Valid latitude must be a number between -90 and 90
func TestDistance_should_error_when_invalid_latitude(t *testing.T) {
	meters, err := Distance(-100, 4.478617, 52.36461880000000235, 4.93169289999999982)

	if err == nil || meters != 0 {
		t.Error("Cant accept latitude outside the valid range")
	}
}

//Valid longitude between -180 and 180.
func TestDistance_should_error_when_invalid_longitude(t *testing.T) {
	meters, err := Distance(52.36461880000000235, -181, 51.925146, 170)
	if err == nil || meters != 0 {
		t.Error("Cant accept longitutde outside the valid range")
	}
}
