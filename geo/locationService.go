package geo

import (
	"bitbucket.org/xild/verbose-pancake/model"
	"bitbucket.org/xild/verbose-pancake/helper"
	"bitbucket.org/xild/verbose-pancake/repository"
	"log"
	"os"
	"time"
	"sync"
)

type (
	LocationService struct {
		Repo    repository.DataAccessRepository
		dataSet *os.File
	}
)

func CreateNewLocationService(repo repository.DataAccessRepository) *LocationService {
	r := new(LocationService)
	r.Repo = repo

	return r
}

// Find the furthesdt and closest location
func (ls *LocationService) GetFurthestAndClosest(defaultLatitude float64, defaultLongitude float64) (furthest []model.Location, closest []model.Location) {
	defer ls.dataSet.Close()
	defer helper.TrackTime(time.Now(), "GetLocation")
	furthest = make([]model.Location, 5)
	closest = make([]model.Location, 5)
	for record := range ls.Repo.SelectAll() {

		meters, err := Distance(record.Latitude, record.Longitude, defaultLatitude, defaultLongitude)

		if err != nil {
			log.Fatal(err)
		}

		record.Distance = meters

		var wg sync.WaitGroup
		wg.Add(2)
		go func() {
			defer wg.Done()

			i := find(len(furthest), func(i int) bool { return furthest[i].Distance <= record.Distance })
			if i > -1 {
				furthest[i] = record
			}
		}()

		go func() {
			defer wg.Done()
			z := find(len(closest), func(i int) bool { return closest[i].Distance >= record.Distance || closest[i].Distance == 0 })
			if z > -1 {
				closest[z] = record
			}
		}()

		wg.Wait()
	}
	return furthest, closest
}

func find(limit int, predicate func(i int) bool) int {
	for i := 0; i < limit; i++ {
		if predicate(i) {
			return i
		}
	}
	return -1
}
