package geo

import (
	"bitbucket.org/xild/verbose-pancake/model"
	"testing"
)

var service *LocationService
var locations []model.Location

func init() {
	service = &LocationService{
		Repo: &TestDAL{},
	}

	locations = []model.Location{
		{ID: 382582, Latitude: 37.1768672, Longitude: -3.608897},
		{ID: 482365, Latitude: 52.36461880000000235, Longitude: 4.93169289999999982},
		{ID: 28403, Latitude: 51.9245615, Longitude: 4.492032399999999},
		{ID: 1098606, Latitude: 52.1542113, Longitude: 4.491864899999999},
		{ID: 24389, Latitude: 50.85132920000000212, Longitude: 5.70318029999999965},
		{ID: 19880, Latitude: 53.2315726000000069, Longitude: 6.52567179999999958},
		{ID: 436807, Latitude: 51.9224817, Longitude: 4.5140063},
		{ID: 420178, Latitude: 50.84924139999999682, Longitude: 5.6903737999999997},
		{ID: 22238, Latitude: 50.8444264, Longitude: 5.7156861},
		{ID: 7818, Latitude: 37.8667538, Longitude: -122.25909899999999},
		{ID: 505868, Latitude: 52.09791479999999808, Longitude: 5.11686619999999959},
		{ID: 381769, Latitude: 52.2934316, Longitude: 4.9934547},
		{ID: 419117, Latitude: 48.8653063, Longitude: 2.3794788},
		{ID: 23928, Latitude: 50.8651879, Longitude: 5.707368199999999},
		{ID: 1049729, Latitude: 52.37165040000000005, Longitude: 4.90306019999999965},
		{ID: 90872, Latitude: 52.06214569999999, Longitude: 4.235672099999999}}
}

type TestDAL struct {
}

func (repo *TestDAL) SelectAll() (ch chan model.Location) {

	ch = make(chan model.Location, 5)
	go func() {
		defer close(ch)
		for _, l := range locations {
			ch <- l
		}
	}()
	return ch
}

func Test_givenValidDataSetShouldReturnClosest(t *testing.T) {

	closestExpected := []struct {
		id  int
		lat float64
		lon float64
	}{
		{28403, 51.924562, 4.492032 },
		{436807, 51.922482, 4.514006 },
		{90872, 52.062146, 4.235672 },
		{381769, 52.293432, 4.993455 },
		{1049729, 52.371650, 4.903060 },
	}

	furthest, closest := service.GetFurthestAndClosest(51.925146,
		4.478617)

	if len(furthest) != 5 {
		t.Fatal("Not top five")
	}

	for _, expected := range closestExpected {
		if contains(closest, expected.id) == false {
			t.Fatalf("Closest Expected missed lon=%f lat=%f Ouput=%v", expected.lon, expected.lat, closest)
		}
	}

}

func Test_givenValidDataSetShouldReturnFurthest(t *testing.T) {
	furthestExpected := []struct {
		id  int
		lat float64
		lon float64
	}{
		{7818, 37.866754, -122.259099 },
		{419117, 48.865306, 2.379479 },
		{22238, 50.844426, 5.715686 },
		{23928, 50.865188, 5.707368 },
		{1049729, 52.371650, 4.903060 },
	}

	furthest, closest := service.GetFurthestAndClosest(51.925146,
		4.478617)

	if len(furthest) != 5 {
		t.Fatal("Not top five")
	}

	for _, expected := range furthestExpected {
		if contains(furthest, expected.id) == false {
			t.Fatalf("Furthest Expected missed lon=%f lat=%f Ouput=%v", expected.lon, expected.lat, closest)
		}
	}

}

func contains(s []model.Location, id int) bool {
	for _, a := range s {
		if a.ID == id {
			return true
		}
	}
	return false
}
