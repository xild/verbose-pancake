package helper

import (
	"log"
	"time"
)

func PanicErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func TrackTime(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s\n", name, elapsed)
}