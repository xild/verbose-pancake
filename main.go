package main

import (
	"bitbucket.org/xild/verbose-pancake/geo"
	"bitbucket.org/xild/verbose-pancake/helper"
	"time"
	"bitbucket.org/xild/verbose-pancake/draw"
	"sync"
	"github.com/olekukonko/tablewriter"
	"os"
	"bitbucket.org/xild/verbose-pancake/model"
	"fmt"
	"bitbucket.org/xild/verbose-pancake/repository"
	"flag"
)

const defaultLatitude float64 = 51.925146
const defaultLongitude float64 = 4.478617

var locationService *geo.LocationService
var drawerService *draw.MapDrawer

func CreateDependency(dataSetType int) {
	repo, err := repository.CreateRepository(dataSetType)
	helper.PanicErr(err)
	locationService = geo.CreateNewLocationService(repo)
	drawerService = draw.Init(defaultLongitude, defaultLatitude, 1000, 1000)
}

var dataSetType int

func main() {
	defer helper.TrackTime(time.Now(), "main")

	flag.IntVar(&dataSetType, "t", 0, `
	data set type
	   1 = IN MEMORY DB
	   0 = CSV
	   default = 0
	`)
	flag.Parse()

	CreateDependency(dataSetType)

	furthest, closest := locationService.GetFurthestAndClosest(defaultLatitude, defaultLongitude)

	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		drawerService.GenerateMap(furthest, "furthest.png")
	}()

	go func() {
		defer wg.Done()
		drawerService.GenerateMap(closest, "closest.png")
	}()

	writeResults(furthest)
	writeResults(closest)

	wg.Wait()

}

func writeResults(locations []model.Location) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"ID", "Latitude", "Longitude", "Distance (M)"})

	for _, v := range locations {
		table.Append([]string{fmt.Sprintf("%d", v.ID), fmt.Sprintf("%2f", v.Latitude), fmt.Sprintf("%2f", v.Latitude), fmt.Sprintf("%2f", v.Distance)})
	}
	table.Render() // Send output
}
