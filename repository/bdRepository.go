package repository

import (
	"bitbucket.org/xild/verbose-pancake/model"
)

type (
	DBAccessLayer struct {
		locations []model.Location
	}
)

func InitDBAccess() *DBAccessLayer {
	d := new(DBAccessLayer)

	return d
}

func (repo *DBAccessLayer) SelectAll() (ch chan model.Location) {
	return
}
