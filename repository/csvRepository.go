package repository

import (
	"bitbucket.org/xild/verbose-pancake/helper"
	"bitbucket.org/xild/verbose-pancake/model"
	"encoding/csv"
	"io"
	"os"
	"strconv"
	"strings"
)

type (

	CSVAccess struct {
		file *csv.Reader
	}
)

func InitCSVDataAccess() *CSVAccess {
	d := new(CSVAccess)

	file, err := os.Open("./resource/geoData.csv")
	r := csv.NewReader(file)

	helper.PanicErr(err)
	d.file = r
	return d
}

func (repo *CSVAccess) SelectAll() (ch chan model.Location) {
	ch = make(chan model.Location, 20)
	go func() {
		r := repo.file
		_, err := r.Read() //read header
		helper.PanicErr(err)

		defer close(ch)
		for {
			record, err := r.Read()
			if err != nil {
				if err == io.EOF {
					break
				}
				helper.PanicErr(err)
			}

			id, err := strconv.Atoi(record[0])
			helper.PanicErr(err)

			latitude, err := strconv.ParseFloat(strings.TrimSpace(record[1]), 64)
			helper.PanicErr(err)

			longitude, err := strconv.ParseFloat(strings.TrimSpace(record[2]), 64)
			helper.PanicErr(err)
			location := model.Location{ID: id, Latitude: latitude, Longitude: longitude}

			ch <- location
		}
	}()
	return
}
