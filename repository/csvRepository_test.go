package repository

import (
	"testing"

	"bitbucket.org/xild/verbose-pancake/model"
	"encoding/csv"
	"strings"
)

func TestDataAccess_SelectAll(t *testing.T) {
	csvs := []string{
		`"id","lat","lng"
382582,37.1768672,-3.608897
482365,52.36461880000000235,4.93169289999999982
28403,51.9245615,4.492032399999999
1098606,52.1542113,4.491864899999999
24389,50.85132920000000212,5.70318029999999965
19880,53.2315726000000069,6.52567179999999958
436807,51.9224817,4.5140063
420178,50.84924139999999682,5.6903737999999997
22238,50.8444264,5.7156861
7818,37.8667538,-122.25909899999999
505868,52.09791479999999808,5.11686619999999959
381769,52.2934316,4.9934547
419117,48.8653063,2.3794788
23928,50.8651879,5.707368199999999`, `"id","lat","lng"
382582,37.1768672,-3.608897
482365,52.36461880000000235,4.93169289999999982`,
	}

	type fields struct {
		reader *csv.Reader
	}

	tests := []struct {
		name           string
		fields         fields
		wantCh         chan model.Location
		lengthExpected int
	}{
		{
			fields:         fields{reader: csv.NewReader(strings.NewReader(csvs[0]))},
			name:           "RETURN ALL LENGHT 14",
			wantCh:         make(chan model.Location, 5),
			lengthExpected: 14,
		},

		{
			fields:         fields{reader: csv.NewReader(strings.NewReader(csvs[1]))},
			name:           "RETURN ALL LENGHT 2",
			wantCh:         make(chan model.Location, 5),
			lengthExpected: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &CSVAccess{
				file: tt.fields.reader,
			}

			var locations []model.Location

			for record := range repo.SelectAll() {
				locations = append(locations, record)
			}

			if len(locations) != tt.lengthExpected {
				t.Fatalf("Match assertion error. Expected=%d instead len=%d", tt.lengthExpected, len(locations))
			}

		})
	}
}
