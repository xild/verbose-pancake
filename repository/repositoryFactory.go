package repository

import (
	"errors"
	"bitbucket.org/xild/verbose-pancake/model"
	"fmt"
)

type DataAccessRepository interface {
	SelectAll() chan model.Location
}

var invalidStructType = errors.New("Invalid DataAcces Type")

const (
	CSV = iota
	BD
)

// CREATE REPOSITORY
// 1 = CSV
// 2 = IN MEMORY DB
func CreateRepository(repositoryType int) (DataAccessRepository, error) {
	switch repositoryType {
	case CSV:
		fmt.Printf("Creating a dataset %v type CSV", CSV)
		csvAccess := InitCSVDataAccess()
		return csvAccess, nil
	case BD:
		fmt.Printf("Creating a dataset %v type BD", BD)
		db := InitDBAccess()
		return db, nil
	default:
		return nil, invalidStructType
	}
}
