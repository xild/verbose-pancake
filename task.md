#Housing Anywhere

We have some listings records in a csv file (geoData.csv, attached) - one listing per line.
We would like to know what top 5 listings are closest (in terms of distance) to our office (GPS coordinates - lat: 51.925146 lng: 4.478617) 
and what 5 listings are the furthest.
Write a program that will read the full list of listings and output two top 5 lists with listings 
IDs and relative distances to our office.
You can use the first formula from this Wikipedia article 
[great circle distance](https://en.wikipedia.org/wiki/Great-circle_distance) to calculate distance,
 don't forget, you'll need to convert degrees to radians.
 Your program should be fully tested too.
Application should be written in Go and core functionality should be covered by unit tests.
 
Additionally, two questions: 
- Please condiser the .csv file to be very big (millions of entries) 
  - how would your application perform?)
- How easy would it be to change your app so it can work with a database instead of files?
  - Would that be possible by passing a couple of params to an app? 
  - (Insigth: )